package com.sofyan.tesapi.repositories;

import com.sofyan.tesapi.models.MUserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository //anotasi untuk class repository
public interface MUserRepository extends JpaRepository<MUserModel, Long> {
    /* class ini untuk menerapkan proses perilaku pengelolaan data dimna
       kita dapat menerapkan perilaku CRUD, query database, custom method query
       dan yang berhubungan dengan pengelolaan perilaku ke database

       yang sering dipakai adalah :
       - Jpa repository
       - Paging and sorting repository
       - Crud repository
     */
}
