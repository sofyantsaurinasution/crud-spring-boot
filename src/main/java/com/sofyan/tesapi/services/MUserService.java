package com.sofyan.tesapi.services;

import com.sofyan.tesapi.dtos.MUserRequest;
import com.sofyan.tesapi.dtos.MUserResponse;
import java.util.List;

public interface MUserService {

     /*
    - class service adalah bussines proccessnya, tpi secara khusus
      class interface ini digunakan untuk menangkap response dari class dto.

     - list tidak perlu memanggil request karena tidak ada perilaku khusus

     - kita harus meng-implementasikan method yang kita inginkan.

     - throws Exception digunakan untuk peng-kondisian logika perilaku jika kondisi salah(tampilkan error)
       jika benar (lanjutkan bisnisnya)
     */

    MUserResponse tambahData (MUserRequest userRequest) throws Exception;

    MUserResponse ubahData (Long id, MUserRequest userRequest) throws Exception;

    List<MUserResponse> tampilkanSemuaData ();

    void hapusData (Long id) throws Exception;

}
