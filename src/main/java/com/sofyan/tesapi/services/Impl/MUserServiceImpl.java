package com.sofyan.tesapi.services.Impl;

import com.sofyan.tesapi.dtos.MUserRequest;
import com.sofyan.tesapi.dtos.MUserResponse;
import com.sofyan.tesapi.models.MUserModel;
import com.sofyan.tesapi.repositories.MUserRepository;
import com.sofyan.tesapi.services.MUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MUserServiceImpl implements MUserService {

    @Autowired
    private MUserRepository userRepository;


    @Override
    public MUserResponse tambahData(MUserRequest userRequest) throws Exception {
        MUserModel addData = new MUserModel(); //anotasi untuk membuat object
        addData.setName(userRequest.getName());
        addData.setUsername(userRequest.getUsername());
        addData.setNik(userRequest.getNik());

        MUserModel saveData = userRepository.save(addData);
        MUserResponse userResponse = new MUserResponse();
        userResponse.setId(saveData.getId());
        userResponse.setName(saveData.getName());
        userResponse.setUsername(saveData.getUsername());
        userResponse.setNik(saveData.getNik());
        return userResponse;
    }

    @Override
    public MUserResponse ubahData(Long id, MUserRequest userRequest) throws Exception {
        Optional<MUserModel> userModel = userRepository.findById(id);
        if (userModel.isEmpty()) {
            throw new Exception("data gak ada njer");
        }
        userModel.get().setName(userRequest.getName());
        userModel.get().setUsername(userRequest.getUsername());
        userModel.get().setNik(userRequest.getNik());

        MUserModel ambilId = userRepository.save(userModel.get());
        MUserResponse response = new MUserResponse();
        response.setId(ambilId.getId());
        response.setName(ambilId.getName());
        response.setUsername(ambilId.getUsername());
        response.setNik(ambilId.getNik());
        return response;
    }

    @Override
    public List<MUserResponse> tampilkanSemuaData() {
        List<MUserModel> findData = userRepository.findAll();
        List<MUserResponse> userResponses = new ArrayList<>();
        findData.forEach(x->{
            MUserResponse mUserResponse = new MUserResponse();
            mUserResponse.setId(x.getId());
            mUserResponse.setName(x.getName());
            mUserResponse.setUsername(x.getUsername());
            mUserResponse.setNik(x.getNik());
            userResponses.add(mUserResponse);
        });
        return userResponses;
    }

    @Override
    public void hapusData(Long id) throws Exception {
    Optional<MUserModel> userModel = userRepository.findById(id);
    if (userModel.isEmpty()){
        throw new Exception("kimochiii");
    }
    userRepository.deleteById(id);
    }
}
