package com.sofyan.tesapi.controllers;

import com.sofyan.tesapi.dtos.MUserRequest;
import com.sofyan.tesapi.dtos.MUserResponse;
import com.sofyan.tesapi.services.MUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
@RestController
public class MUserController {
    /*
     - @RestController adalah hasil akhir dari pembuatan rest api berupa data Json
     - @RequestMapping adalah anotasi untuk membuatan url
     - @Controller biasanya digunakan untuk api retrun halaman HTML yang biasanya
       digunakan sebagai perantara frontend
     - @RequestBody untuk menampilkan data ke halaman
     - @PathVariable digunakan untuk membaca lokasi field
     */

    @Autowired
    private MUserService userService;

    @PostMapping
    public MUserResponse tambahData (@RequestBody MUserRequest userRequest) throws Exception{
        return userService.tambahData(userRequest);
    }

    @PutMapping ("/{id}")
    public MUserResponse ubahData (@PathVariable(value = "id") Long id,@RequestBody MUserRequest userRequest) throws Exception{
        return userService.ubahData(id, userRequest);
    }

    @GetMapping
    public List<MUserResponse> tampilkanSemuaData (){
        return userService.tampilkanSemuaData();
    }

    @DeleteMapping ("/{id}")
    public String hapusData (@PathVariable(value = "id") Long id) throws Exception{
        userService.hapusData(id);
        return "arigato";
    }
}
