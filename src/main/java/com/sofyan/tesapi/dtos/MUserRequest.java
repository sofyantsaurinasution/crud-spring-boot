package com.sofyan.tesapi.dtos;

import lombok.Data;

@Data //anotasi untuk generate get,set
public class MUserRequest {

    /*
    1. dto = data transfer objek
    2. dto adalah sebuah package yang menggambarkan perilaku request dan response data dari model
       - request artinya adalah meminta/menambah kan data ke table database melalui perantara package model yang
         yang terdiri dari beberapa field yang tidak ada id didalamnya

       - response artinya adalah balasan/feedback dari model (tabel database) yang akan diteruskan oleh class response
         dan akan ditampilkan sesuai dengan return {hasil} yang diinginkan

       - model akan selalu berpasangan dengan response
     */
    private String username; //field
    private String name; //field
    private String nik; //field
}
