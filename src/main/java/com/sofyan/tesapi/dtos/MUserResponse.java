package com.sofyan.tesapi.dtos;

import lombok.Data;

@Data //anotasi untuk generate get,set
public class MUserResponse {

    private Long id; //field
    private String username; //field
    private String name; //field
    private String nik; //field
}
