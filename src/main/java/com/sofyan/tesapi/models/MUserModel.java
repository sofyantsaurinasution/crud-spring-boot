package com.sofyan.tesapi.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity //anotasi yang mendefenisikan class sebagai tabel di database
@Data // anotasi untuk generate get,set,toString
@AllArgsConstructor //anotasi untuk generate constructor
@NoArgsConstructor //anotasi untuk generate contsturctor kosong
@Table (name = "m_user") //anotasi untuk untuk custom nama tabel
public class MUserModel {

    @Id //anotasi ini wajib ada melambang kan primary key yaitu user_id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //anotasi untuk auto increment
    @Column (name = "user_id", nullable = false, length = 12) //anotasi ini untuk custom column
    private Long id; //field

    @Column (name = "username", nullable = false, length = 20) //anotasi ini untuk custom column
    private String username; //field

    @Column (name = "name", nullable = false, length = 20) //anotasi ini untuk custom column
    private String name;//field

    @Column (name = "nik", nullable = false, length = 20) //anotasi ini untuk custom column
    private String nik;//field
}
