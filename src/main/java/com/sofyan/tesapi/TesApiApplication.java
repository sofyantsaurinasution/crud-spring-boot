package com.sofyan.tesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesApiApplication.class, args);
	}

}
